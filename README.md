# Document Signer Utility

1. Setup a Document Signer from the Settings Tab in your Leegality Dashboard, by entering your application URL. For example, if you are deploying your application at docsigner.example.com, please save the same in the URL Field. Alternatively, you can start by entering any demo URL and update the same later. 

2. On successful set, you will receive an eSign ID. 

3. Download *docsigner.war*.

4. Please ensure that Java is installed on your server. (Recommended version: Java 17.0.11)

5. Setup TOMCAT 10.1.23 or above on your server- Read [here](https://www.digitalocean.com/community/tutorials/how-to-install-apache-tomcat-10-on-ubuntu-20-04) to know how.

6. After installing Tomcat, go to *webapps* directory and place the *docsigner.war* file in it. Please ensure that you do not change the name of the WAR file. 

7. After placing WAR file, restart the TOMCAT. WAR will be exploded in the same folder.

8. Go to the folder *docsigner*. Locate the *certificate.properties* file (Create a file with the same name if you cannot locate the file).
9. Open the *certificate.properties*  file to configure Document Signer Certificate and add the following lines to it
    
    ```
    # Enables request and response logging
    doc.signer.logs.enabled=true

    # PKCS12 keystore file path
    doc.signer.certificate.< eSign ID >.path=Your PKCS12 keystore file path here

    # PKCS12 keystore password
    doc.signer.certificate.< eSign ID >.password=PKCS12 file password
    ```
    
    Where < eSign ID > is your eSign ID generated from the Leegality dashboard above. You can configure multiple .pfx files in the same application instance by simply adding multiple configurations.
    
10. Application will run at **/docsigner**. Please ensure that it does not conflict with any other application.

11. Health check endpoint is **docsigner/actuator/health**. Please use this endpoint for docsigner observbility. 
    Please use the above endpoint like **https://abc.example.com/docsigner/actuator/health**

Now, whenever a signing request is received for your eSign ID, the document will be eSigned using your Document Signer Certificate.

You can further configure Automated Signing using Document Signer by creating a passkey for the same in your Leegality Dashboard. Pursuant to the same, users from your Organisational Department would be able to get documents eSigned using Document Signer Certificate either using our API Gateway or using the Leegality Dashboard.

**Logs:** All communication with your application is loggable and is enabled by default. You have option to disable the same but that is not recommended. The logs will be created in catalina.out file in TOMCAT logs Directory. You can also change the destination if you wish so. See this link for more details.
    
    Format: yyyy-MM-dd HH:mm:ss.SSS INFO PID --- [THREAD_NAME] APP_PACKAGE_NAME: Message

    Examples:
    
    2024-04-26 17:05:21 2024-04-26T11:35:21.886Z  INFO 1 --- [nio-8080-exec-4] c.g.d.filters.ApplicationLoggerFilter    : Incoming request [ /docsigner/sr/status?pathId=HLVt8dc ] with payload 
    
    2024-04-26 17:05:22 2024-04-26T11:35:22.465Z  INFO 1 --- [nio-8080-exec-4] c.g.d.filters.ApplicationLoggerFilter    : Sending response request [ /docsigner/sr/status?pathId=HLVt8dc ] with body {"status":1,"commonName":"DS Grey Swift Private Limited Test 01","certificateExpiry":"2026-04-14T07:48:04.000+00:00"}
